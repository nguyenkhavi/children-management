from threading import *
import os
import time

import core
from configuration import *

TIME_PATTERN="%d_%m_%Y_%H_%M_%S"
SECONDS_PER_DAY=24*60*60

def processPeriod(period):

    if period['duration']==None:
        period['duration']=SECONDS_PER_DAY
    if period['interrupt']==None:
        period['interrupt']=0
    if period['sum']==None:
        period['sum']=SECONDS_PER_DAY
    #convert to second
    start=int(period['from'])/100
    period['from']= int(start/100)*3600+int(start%100)*60+int(period['from'])%100
    end=int(period['to'])/100
    period['to']= int(end/100)*3600+int(end%100)*60+int(period['to'])%100
    return period

def convertTimeToString(t):
    hh=int(t/3600)
    mm=int((t-hh*3600)/60)
    if mm<10:
        mm = "0" + str(mm)
    ss=t-int(hh)*3600-int(mm)*60
    if ss<10:
        ss =  "0" + str(ss)
    return str(hh)+":"+str(mm)+ ":" +str(ss)

class SupervisorThread(Thread):
    def __init__(self,child_id,msgWidget):
        
        self.RUNNING=True
        self.child_id=child_id
        self.msgWidget=msgWidget
        self.config=getConfig()
        super(SupervisorThread,self).__init__()
        
    
    def run(self):
        
        while (self.RUNNING):
            #check in time
            period=self.isActiveTime()
            if period=='':
                #call shutdown
                Timer(1,self.shutdownFunction).start()
                break
            '''
            check valid time=check interruped + check duration + check total + is valid time
            '''
            #process time in period
            period=processPeriod(period=period)
            
            #set end time
            endtime=int(period['to'])
            self.msgWidget.setText("You can not using computer after: "+convertTimeToString(endtime))
            
            #test duration period
            period['duration']=60
            period['interrupt']=60

            print(period)
            total=int(period['sum'])
            interrupt=int(period['interrupt'])
            duration=int(period['duration'])

            #update config if new day
            oldtime=datetime.strptime(str(self.config.get('CONFIG','lastusing')),TIME_PATTERN)
            currenttime=datetime.now()

            #check time if 5 minute left to shutdown
            crtt=currenttime.time().hour*3600+currenttime.time().minute*60+currenttime.second
            if (endtime-crtt)<=5*60:
                self.msgWidget.setText("You have left < 5 minute until "+convertTimeToString(endtime))
            if(str(currenttime.date())!=str(oldtime.date())):
                self.config.set('CONFIG','usingtime',str(0))
                self.config.set('CONFIG','startusing',str(currenttime.strftime(TIME_PATTERN)))
                self.config.set('CONFIG','lastusing',str(currenttime.strftime(TIME_PATTERN)))
                writeConfig(self.config)
                    
            usingtime=int(self.config.get('CONFIG','usingtime'))
            #check total
            # if usingtime>=total:
            #     #call shutdown timer
            #     Timer(1,self.shutdownFunction).start()
                
            #     break
            #check duration

            # if (usingtime%duration)==0:
            #     #caculate interrupt
            #     if getInterruptTime(config=self.config)<=interrupt:
            #         #call log off
            #         Timer(1,self.logoffFunction).start()
            #         break
            #     pass
            #update period
            if (str(period["_id"])!=str(self.config.get('CONFIG','periodid'))):
                
                lastusing=datetime.now().strftime(TIME_PATTERN)
                
                self.config.set('CONFIG','periodid',str(period['_id']))
                self.config.set('CONFIG','usingtime',str(0))
                self.config.set('CONFIG','lastusing',str(lastusing))
                writeConfig(self.config)
            else:
                lastusing=datetime.now().strftime(TIME_PATTERN)
                usingtime+=1*60
                self.config.set('CONFIG','usingtime',str(usingtime))
                self.config.set('CONFIG','lastusing',str(lastusing))
                writeConfig(self.config)


            #screenshot and up to database
            self.handleCreateScreenshot()
            #sleep 60 second
            print("Starting sleep thread 60 secs")
            time.sleep(1*60)
        
        #call shutdown after thread not runs
        Timer(1,self.shutdownFunction).start()
    
    #get current period
    def isActiveTime(self):
        periods = core.getPeriodsByChildId(child_id=self.child_id)
        # print(periods)
        current = core.time_to_number(datetime.now().time())

        isOk = False
        retPeriod=""
        for period in periods:
            if period["from"] <= current and current <= period["to"]:
                isOk = True
                retPeriod=period
                break

        return retPeriod

    
    #screen shot function
    def handleCreateScreenshot(self):
        filename = core.takeScreenshot(self.child_id)
        try:
            url = core.uploadFile(file_name=filename)
            core.createScreenshotByChildId(child_id=self.child_id, url=url)
        except Exception as e:
            print(f'Error upload file:{e}')

    
    def cancel(self):
        self.RUNNING=False
        print("SUPERVISOR DONE")

    def logoffFunction(self):
        os.system("shutdown -l")
    
    def shutdownFunction(self):
        print("shut down")
        # os.system("shutdown /s /t 0")