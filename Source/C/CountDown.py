from PyQt5 import *
from PyQt5.QtWidgets import *
from threading import Thread
import time
from PyQt5 import uic
from pyautogui import countdown

class CountDown(QDialog):
    def __init__(self,time,msg) -> None:
        super(CountDown,self).__init__()
        uic.loadUi('annouce.ui',self)
        
        self.msgLable=self.findChild(QLabel,'msgLable')
        self.msgLable.setText("")
        self.show()
        self.time=time
        self.msg=msg
        self.IS_RUNNING=True
        #create thread
        

    def countDown(self):
        while(self.IS_RUNNING):
            # print("Is RUNNING ",self.IS_RUNNING)
            while (self.time>0):
                time.sleep(1)
                self.time-=1
                self.msgLable.setText(self.msg+str(self.time))
                print(self.msg+str(self.time))
        print("Task dONE")
        self.close()

    def setTime(self,time):
        if(time<=0):
            time=0
            self.time=time
            self.hide()
            return
        self.time=time
        self.show()
        
    
    def destroyCountDown(self):
        self.time=0
        self.IS_RUNNING=False
        
        print("Count down is done")

    def createCountDown(self,time):
        self.time=time
        self.countDownThread=Thread(name="countdown",target=self.countDown)
        self.countDownThread.start()
        