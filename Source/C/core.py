from datetime import time
from datetime import datetime
import os
import random
import pyautogui
import hashlib
from bson.objectid import ObjectId

import config
accountModel = config.getAccountModel()
childModel = config.getChildModel()
periodModel = config.getPeriodModel()
screenshotModel = config.getScreenshotModel()

bucket = config.getBucket()
'''
Usage:
# print(login(username="nguyenkhavi", password="pass@123"))
# print(createChildByParentId(
#     parent_id='61d923ee384a86f77242236f', name="Kha Vi", age=8))
# print(deleteChildById(id='61d9277e46dcb42e381cec5e'))
# print(createPeriodByChildId(child_id='61d929e00b52da8e5930e9dd',
#       fro=time(12, 30), to=time(16, 30)))
# print(updatePeriodById(id='61d92d2ac62fc31e876f800d', fro=time(0, 30)))
# print(deletePeriodById(id='61d92d2ac62fc31e876f800d'))
# print(createScreenshotByChildId(child_id='61d929e00b52da8e5930e9dd', url='url.com'))
# print(uploadFile(file_name='main.ui'))
# print(getPeriodsByChildId(child_id='61d929e00b52da8e5930e9dd'))
# print(getScreenshotsByChildId(child_id='61d929e00b52da8e5930e9dd'))

'''
TIME_PATTERN="%d_%m_%Y_%H_%M_%S"

def takeScreenshot(id=""):
    filename = f'{id}_{datetime.now().strftime(TIME_PATTERN)}.png'
    filename="screenshot/"+filename
    pyautogui.screenshot().save(filename)
    return filename


def saveAccount(id):
    with open("ACCOUNT_CONFIG.txt", "w") as f:
        f.write(id)


def uploadFile(*, file_name):
    blob = bucket.blob(file_name)
    blob.upload_from_filename(filename=file_name)
    os.remove(file_name)
    blob.make_public()
    return blob.public_url


'''input 12:00'''


def text_to_time(text):
    hhmm = [int(i) for i in text.split(':')]
    return time(hhmm[0], hhmm[1])


def time_to_number(time):
    return 10000*time.hour + 100*time.minute + time.second


def hash(password):
    salt = os.urandom(32)
    return salt + hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)


def verify(password, hashed):
    salt = hashed[:32]  # 32 is the length of the salt
    key = hashed[32:]
    new_key = hashlib.pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),  # Convert the password to bytes
        salt,
        100000
    )
    return key == new_key


def createAccount(*, username=None, password=None):
    if username == None or password == None:
        raise ValueError('Username and password must be provided!')

    old = accountModel.find_one({"username": username})

    '''Username exists'''
    if old != None:
        raise ValueError('Username exists!')

    hashed = hash(password)
    collection = {
        "username": username,
        "password": hashed
    }
    id = accountModel.insert_one(collection).inserted_id
    return accountModel.find_one({"_id": id})


def login(*, username=None, password=None):
    old = accountModel.find_one({"username": username})

    '''Username does not exists'''
    if old == None:
        raise ValueError('Username does not exists!')
    is_correct = verify(password=password, hashed=old['password'])

    '''Password is incorrect'''
    if not is_correct:
        raise ValueError('Username and password is incorrect!')
    del old['password']
    return old


def createChildByParentId(*, parent_id=None, name=None, age=None):
    if parent_id == None or name == None:
        raise ValueError('Name must be provided!')

    parent = accountModel.find_one({"_id": ObjectId(parent_id)})
    if parent == None:
        raise ValueError('Parent does not exists!')

    collection = {
        "parent_id": parent_id,
        "name": name,
        "age": age,
    }
    id = childModel.insert_one(collection).inserted_id
    return childModel.find_one({"_id": id})


def isValidChild(*, id):
    child = childModel.find_one({"_id": ObjectId(id)})
    if child == None:
        raise ValueError("Child's ID does not exist!")
    return child


def getChildrenByParentId(*, parent_id):
    parent = accountModel.find_one({"_id": ObjectId(parent_id)})
    if parent == None:
        raise ValueError('Parent does not exists!')
    query = {'parent_id': parent_id}
    return list(childModel.find(query))


def deleteChildById(*, id):
    return childModel.delete_one({"_id": ObjectId(id)})


def getPeriodsByChildId(*, child_id=None):
    child = childModel.find_one({"_id": ObjectId(child_id)})
    if child == None:
        raise ValueError('Child does not exists!')
    query = {'child_id': child_id}
    return list(periodModel.find(query))


def createPeriodByChildId(*, child_id=None, fro=None, to=None, duration=None, interrupt=None, sum=None):
    if fro == None or to == None or child_id == None:
        raise ValueError('From-time, to-time and child"s id must be provided!')

    child = childModel.find_one({"_id": ObjectId(child_id)})
    if child == None:
        raise ValueError('Child does not exists!')

    collection = {
        "child_id": child_id,
        "from": time_to_number(fro),
        "to": time_to_number(to),
        "duration": duration,
        "interrupt": interrupt,
        "sum": sum
    }
    id = periodModel.insert_one(collection).inserted_id
    return periodModel.find_one({"_id": id})


def updatePeriodById(*, id=None, fro=None, to=None, duration=None, interrupt=None, sum=None):
    old = periodModel.find_one({"_id": ObjectId(id)})
    if old == None or id == None:
        raise ValueError('Period does not exists!')

    query = {"_id": ObjectId(id)}
    new_value = {}
    if fro != None:
        new_value['from'] = time_to_number(fro)
    if to != None:
        new_value['to'] = time_to_number(to)
    if duration != None:
        new_value['duration'] = duration
    if interrupt != None:
        new_value['interrupt'] = interrupt
    if sum != None:
        new_value['sum'] = sum
    periodModel.update_one(query, {'$set': new_value})

    return periodModel.find_one({"_id": ObjectId(id)})


def deletePeriodById(*, id=None):
    old = periodModel.find_one({"_id": ObjectId(id)})
    if old == None or id == None:
        raise ValueError('Period does not exists!')

    periodModel.delete_one({"_id": ObjectId(id)})

    return periodModel.find_one({"_id": ObjectId(id)})


def createScreenshotByChildId(*, child_id=None, url=None):
    if url == None:
        raise ValueError('Url must be provided!')
    child = childModel.find_one({"_id": ObjectId(child_id)})
    if child == None:
        raise ValueError('Child does not exists!')
    collection = {
        "child_id": child_id,
        "url": url,
        "created_at": datetime.utcnow()
    }
    id = screenshotModel.insert_one(collection).inserted_id
    return screenshotModel.find_one({"_id": id})


def getScreenshotsByChildId(*, child_id=None):
    child = childModel.find_one({"_id": ObjectId(child_id)})
    if child == None:
        raise ValueError('Child does not exists!')
    query = {'child_id': child_id}
    return list(screenshotModel.find(query))
