
import configparser
from re import I, L
import re
import sys
from time import time
from typing import List
from PyQt5 import QtGui, QtWidgets, uic, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import os

from SupervisorThread import *
from datetime import datetime
from threading import Timer
from configuration import *
from CountDown import *

INTERVAL_SHUTDOWN_PARENT = 15
INTERVAL_SHUTDOWN_CHILDREN = 15
INTERVAL_ASK_PARENT = 60*60
INTERVAL_ASK_CHILDREN = 15
INTERVAL_SCREENSHOT = 5
IS_DEV = True


class RepeatTimer(Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


class MyApp(QMainWindow):
    def __init__(self) -> None:
        super(MyApp, self).__init__()
        # set up everything need
        try:
            if os.path.isfile("ACCOUNT_CONFIG.txt"):
                with open('ACCOUNT_CONFIG.txt', 'r') as f:
                    self.child_id = f.readline()
                core.isValidChild(id=self.child_id)
                self.connectToLoginWidgets()
                # set up state of ui
                self.setFocusPolicy(Qt.FocusPolicy.StrongFocus)
                self.setFixedSize(self.size())
                self.setWindowFlags(Qt.WindowType.WindowStaysOnTopHint)
                self.setWindowTitle("Child app")
                self.setUpEnvironmentVariables()
                
                # create thread
                self.timer = Timer(
                    INTERVAL_SHUTDOWN_CHILDREN, self.shutdownFunction)
                self.timer.start()
                #create count down dialog
                self.countDownDiag=CountDown(INTERVAL_SHUTDOWN_CHILDREN,"Left time:")
                self.countDownDiag.show()
                self.countDownDiag.createCountDown(INTERVAL_SHUTDOWN_CHILDREN)
            else:
                raise ValueError("Config file does not exist!")
        except Exception as e:
            print(e)
            self.connectSetupWidgets()

    def setUpEnvironmentVariables(self):
        self.IS_PARENT = False
        self.IS_CHILDREN = False
        self.CAN_SHUTDOWN = True
        self.INPUT_TIME = 0
        self.IS_RUNNING=True
        self.IS_COUNTDOWN=True
        self.supervisor = None
        self.timer = None
        self.askTimer = None

    def connectToLoginWidgets(self):
        uic.loadUi('main.ui', self)
        self.username = self.findChild(QLineEdit, 'username')
        self.password = self.findChild(QLineEdit, 'password')
        self.sign_in = self.findChild(QPushButton, 'sign_in')
        self.sign_in_child = self.findChild(QPushButton, 'sign_in_child')

        self.err_msg = self.findChild(QLabel, 'err_msg')
        self.err_msg.setText("")
        print(self.username, self.password)
        self.sign_in.clicked.connect(self.handleLogin)
        self.sign_in_child.clicked.connect(self.handleChildLogin)

    #get current period
    def isActiveTime(self):
        periods = core.getPeriodsByChildId(child_id=self.child_id)
        # print(periods)
        current = core.time_to_number(datetime.now().time())

        isOk = False
        retPeriod=""
        for period in periods:
            if period["from"] <= current and current <= period["to"]:
                isOk = True
                retPeriod=period
                break

        return retPeriod

    def handleChildLogin(self):
        print('child login')
        period=self.isActiveTime()
        if period=='':
            self.err_msg.setText(
                "Not an accepted time, contact your parent and re-open computer later!")
            #wait for shutdown

        else:
            self.err_msg.setText(
                "Logged in!")
            self.handleLoginSuccess(isChild=True,period=period)

    def handleCreateScreenshot(self):
        filename = core.takeScreenshot(self.child_id)
        try:
            url = core.uploadFile(file_name=filename)
            core.createScreenshotByChildId(child_id=self.child_id, url=url)
        except Exception as e:
            print(f'Error upload file:{e}')

    def handleLogin(self):
        child_id = self.child_id
        try:
            child = core.isValidChild(id=child_id)
            username = self.username.text()
            password = self.password.text()
            parent = core.login(username=username, password=password)
            if str(child['parent_id']) != str(parent['_id']):
                raise ValueError("Wrong parent account!")
            print("Logged in")
            self.err_msg.setText(
                "Parent password is confirmed!\nWe will ask password again after 60 minutes!")

            self.handleLoginSuccess()
        except Exception as e:
            self.err_msg.setText(str(e))

    def connectSetupWidgets(self):
        uic.loadUi('setup.ui', self)
        self.username = self.findChild(QLineEdit, 'username')
        self.password = self.findChild(QLineEdit, 'password')
        self.id = self.findChild(QLineEdit, 'id')
        self.setup = self.findChild(QPushButton, 'setup')
        self.err_msg = self.findChild(QLabel, 'err_msg')
        self.err_msg.setText("")
        self.setup.clicked.connect(self.handleSetup)

    def handleSetup(self):
        child_id = str(self.id.text())
        try:
            core.isValidChild(id=child_id)
            username = self.username.text()
            password = self.password.text()
            core.login(username=username, password=password)
            core.saveAccount(child_id)
            self.err_msg.setText("Logged in successfully. Please re-open app!")
        except Exception as e:
            self.err_msg.setText(str(e))

    # prevent children close program
    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        # uncomment this to prevent (if can not close use task manager or system monitor)
        # a0.ignore()
        if(self.supervisor!=None): 
            self.supervisor.cancel()
        if(self.timer!=None): 
            self.timer.cancel()
        
        # self.countDownDiag.destroyCountDown()
        # if self.askTimer!=None:
        #     self.askTimer.cancel()
        
        self.setWindowState(Qt.WindowState.WindowMinimized)


    def handleLoginSuccess(self, isChild=False,period=None):
        if(self.CAN_SHUTDOWN):
            #disable widgets to prevent login mess
            # self.sign_in.setEnabled(False)
            # self.sign_in_child.setEnabled(False)

            self.IS_PARENT = not isChild
            self.IS_CHILDREN = isChild
            self.INPUT_TIME = 0
            # cancel children timer
            self.timer.cancel()
            del self.timer
            self.countDownDiag.destroyCountDown()
            
            interval_shutdown = INTERVAL_SHUTDOWN_CHILDREN if isChild else INTERVAL_SHUTDOWN_PARENT
            self.timer = Timer(interval_shutdown, self.shutdownFunction)
            
            self.CAN_SHUTDOWN = False
            # create thread
            interval_ask = INTERVAL_ASK_CHILDREN if isChild else INTERVAL_ASK_PARENT

            self.askTimer = Timer(interval_ask, self.askPasswordAgain)
            self.askTimer.start()
            print(f'thread isChild={isChild} is created')
            if self.IS_PARENT:
                isChild=False
                if self.supervisor!=None:
                    self.supervisor.cancel()
            #take screenshot if is children
            if isChild:
                self.supervisor=SupervisorThread(child_id=self.child_id,msgWidget=self.err_msg)
                self.supervisor.start()


    def askPasswordAgain(self):
        self.IS_PARENT = False
        self.CAN_SHUTDOWN = True
        self.setWindowState(Qt.WindowState.WindowMaximized)
        print("State change")
        self.timer.start()
        self.countDownDiag.createCountDown(INTERVAL_ASK_CHILDREN)
        # self.screenshotTimer.cancel()
        self.err_msg.setText(
            "You have 15 seconds to input password or computer will be shutdown!")
    def shutdownFunction(self):
        if(self.CAN_SHUTDOWN):
            # call shutdown here
            if not IS_DEV :
                os.system("shutdown -l")
                print("Computer is shutdown")

    


def window():
    app = QApplication(sys.argv)
    win = MyApp()
    win.show()
    app.exec_()

SECONDS_PER_DAY=24*60*60

def processPeriod(period):

    if period['duration']==None:
        period['duration']=SECONDS_PER_DAY
    if period['interrupt']==None:
        period['interrupt']=0
    if period['sum']==None:
        period['sum']=0
    return period

window()
