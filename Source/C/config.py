
'''
Firebase
'''
import pymongo
import firebase_admin
import requests
from firebase_admin import credentials, storage, firestore

bucket=""
try:
    cred = credentials.Certificate('firebase.json')
    app = firebase_admin.initialize_app(
        cred, {"storageBucket": "children-management-72b30.appspot.com"})

    bucket = storage.bucket()
except Exception as e:
    print(e)


def getBucket():
    return bucket


'''
Database
'''


mongo_uri = 'mongodb+srv://nguyenkhavi:UOjjTNgrEGsk6TDh@cluster0.vo4ad.mongodb.net'
my_client = pymongo.MongoClient(mongo_uri)
my_db = my_client["children-management"]


def getAccountModel():
    return my_db["account"]


def getChildModel():
    return my_db["child"]


def getPeriodModel():
    return my_db["period"]


def getScreenshotModel():
    return my_db["screenshot"]
