from PyQt5 import uic, QtGui
from PyQt5.QtCore import *
import sys
import os
import core
from datetime import time
from datetime import datetime
from PyQt5.QtWidgets import *
from threading import Timer, Thread
import time

POLL_INTERVAL = 60


class RepeatTimer(Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)

class Window(QMainWindow):
    def __init__(self) -> None:
        super(Window, self).__init__()
        # check config exist
        # if os.path.isfile("ACCOUNT_CONFIG.txt"):
        #     with open('ACCOUNT_CONFIG.txt', 'r') as f:
        #         self.userid = f.readline()
        #         print(self.userid)
        #     self.connectWidgetsMain()
        # else:
        #     self.connectWidgetsSignUp()
        try:
            if os.path.isfile("ACCOUNT_CONFIG.txt"):
                with open('ACCOUNT_CONFIG.txt', 'r') as f:
                    self.userid = f.readline()
                    core.isValidParent(id=self.userid)
                self.connectWidgetsMain()
            else:
                raise ValueError("Config file does not exist!")
        except Exception as e:
            print(e)
            self.connectWidgetsSignUp()
        # if os.path.isfile("ACCOUNT_CONFIG.txt"):
        #     with open('ACCOUNT_CONFIG.txt', 'r') as f:
        #         self.userid = f.readline()
        #         core.isValidParent(id=self.userid)
        #     self.connectWidgetsMain()
        self.show()

    def connectWidgetsMain(self):
        self.ui = uic.loadUi('main.ui', self)
        self.tabdisplay = self.findChild(QTabWidget, 'tabdisplay')

        self.childrenlist = self.findChild(QWidget, 'childrenlist')

        self.schedule = self.findChild(QWidget, 'schedule')

        self.log = self.findChild(QWidget, 'log')

        childlist = core.getChildrenByParentId(parent_id=self.userid)
        self.childlist = childlist
        self.renderSelect()

        self.tabdisplay.setTabText(0, 'Chilren list')
        self.tabdisplay.setTabText(1, 'Schedule')
        self.tabdisplay.setTabText(2, 'Log')

        '''Chilren list'''
        self.childinput = self.findChild(QLineEdit, 'childinput')
        self.addnewchild = self.findChild(QPushButton, 'addnewchild')
        self.childrentable = self.findChild(QTableWidget, 'childrentable')

        self.addnewchild.clicked.connect(self.addNewChild)
        self.renderChildrenTable()

        '''Schedule'''
        self.select = self.findChild(QComboBox, 'select')
        self.select.currentIndexChanged.connect(self.handleChangeChild)

        self.fro = self.findChild(QTimeEdit, 'from')

        self.to = self.findChild(QTimeEdit, 'to')

        self.duration = self.findChild(QLineEdit, 'duration')
        self.interrupt = self.findChild(QLineEdit, 'interrupt')
        self.sum = self.findChild(QLineEdit, 'sum')
        self.period_table = self.findChild(QTableWidget, 'period_table')
        self.create_new_period = self.findChild(
            QPushButton, 'create_new_period')
        self.err_msg = self.findChild(QLabel, 'err_msg')
        self.err_msg.setText("")
        self.create_new_period.clicked.connect(self.handleCreateNewPeriod)
        self.renderPeriodTable()

        '''Log'''
        self.image = self.findChild(QLabel, 'image')
        self.prevBtn = self.findChild(QPushButton, 'previous')
        self.prevBtn.clicked.connect(self.handlePrevious)
        self.nextBtn = self.findChild(QPushButton, 'next')
        self.nextBtn.clicked.connect(self.handleNext)
        self.select_log = self.findChild(QComboBox, 'select_log')
        self.select_log.currentIndexChanged.connect(self.handleChangeChild)
        self.refreshBtn = self.findChild(QPushButton, 'refreshBtn')
        self.date = self.findChild(QLabel, 'date')
        self.refreshBtn.clicked.connect(self.refresh)
        self.renderImage()
        # self.pollTimer = RepeatTimer(POLL_INTERVAL, self.refresh)
        # self.pollTimer.start()

    def renderSelect(self):
        '''clear'''
        childlist = core.getChildrenByParentId(parent_id=self.userid)
        self.childlist = childlist
        self.select.clear()
        self.select_log.clear()
        for child in self.childlist:
            text = child["name"] + " - " + str(child["_id"])
            self.select.addItem(text)
            self.select_log.addItem(text)

    def refresh(self):
        print('refresh')
        self.err_msg.setText("")
        self.renderPeriodTable()
        self.err_msg_log.setText("")
        self.renderImage()
        self.renderChildrenTable()

    def handleChangeChild(self):

        # try:
        #     self.changeTimer.cancel()
        # except:
        #     pass
        # self.changeTimer = QThread(self.refresh)
        # self.changeTimer.start()
        self.refresh()

    def renderImage(self):
        try:
            child_id = self.select_log.currentText().split(" - ")[1]
            self.err_msg_log.setText("")

        except Exception as e:
            self.err_msg_log.setText("Please select a child!!")
            return
        screenshots = core.getScreenshotsByChildId(child_id=child_id)
        images = []
        for screenshot in screenshots:
            url = screenshot["url"]
            created_at = screenshot['created_at']
            local_url = core.download(url)
            images.append({
                "name": local_url,
                "date": created_at
            })
        self.screenshots = images
        self.screenshotIdx = None
        if len(self.screenshots) > 0:
            self.screenshotIdx = 0
            self.image.setPixmap(QtGui.QPixmap(
                self.screenshots[self.screenshotIdx]["name"]))
            self.date.setText(
                core.utc_time_to_string(
                    self.screenshots[self.screenshotIdx]["date"]))
        else:
            self.err_msg_log.setText("No image!")
            self.date.setText("_")
            self.image.clear()

    def handlePrevious(self):
        try:
            prev = self.screenshotIdx - 1
            image = self.screenshots[prev]
            self.screenshotIdx = prev
            self.image.setPixmap(QtGui.QPixmap(
                image["name"]))
            self.date.setText(core.utc_time_to_string(image["date"]))

        except Exception as e:
            pass

    def handleNext(self):
        try:
            prev = self.screenshotIdx + 1
            image = self.screenshots[prev]
            self.screenshotIdx = prev
            self.image.setPixmap(QtGui.QPixmap(
                image["name"]))
            self.date.setText(core.utc_time_to_string(image["date"]))

        except Exception as e:
            pass

    def handleCreateNewPeriod(self):
        try:
            child_id = self.select.currentText().split(" - ")[1]
            self.err_msg.setText("")
        except:
            self.err_msg.setText("Please select a child!!")
            return

        fro = core.text_to_time(self.fro.text())
        to = core.text_to_time(self.to.text())

        duration = int(self.duration.text()
                       ) if self.duration.text().isnumeric() else None
        interrupt = int(self.interrupt.text()
                        ) if self.interrupt.text().isnumeric() else None
        sum = int(self.sum.text()) if self.sum.text().isnumeric() else None
        core.createPeriodByChildId(
            child_id=child_id, fro=fro, to=to, duration=duration, interrupt=interrupt, sum=sum)
        self.renderPeriodTable()

    def addNewChild(self):
        name = str(self.childinput.text())
        core.createChildByParentId(parent_id=self.userid, name=name)
        self.renderChildrenTable()
        self.renderSelect()

    def renderChildrenTable(self):
        # self.setCentralWidget(self.childrentable)
        childlist = core.getChildrenByParentId(parent_id=self.userid)
        self.childlist = childlist
        self.childrentable.setColumnCount(3)
        self.childrentable.setRowCount(len(childlist))
        for index, child in enumerate(childlist):
            nameItem = QTableWidgetItem(child["name"])
            self.childrentable.setItem(index, 0, nameItem)
            idItem = QTableWidgetItem(str(child["_id"]))
            self.childrentable.setItem(index, 1, idItem)
            deleteBtn = QPushButton('Xóa')
            deleteBtn.clicked.connect(self.handleDeleteChild)
            self.childrentable.setCellWidget(index, 2, deleteBtn)

    def renderPeriodTable(self):
        # self.setCentralWidget(self.childrentable)
        try:
            child_id = self.select.currentText().split(" - ")[1]
        except:
            self.err_msg.setText("Please select a child!!")
            return
        periods = core.getPeriodsByChildId(child_id=child_id)
        self.periods = periods
        self.period_table.setColumnCount(6)
        self.period_table.setRowCount(len(periods))
        for index, period in enumerate(periods):
            self.period_table.setItem(
                index, 0, QTableWidgetItem(str(period["from"])))
            self.period_table.setItem(
                index, 1, QTableWidgetItem(str(period["to"])))
            self.period_table.setItem(
                index, 2, QTableWidgetItem(str(period["duration"])))
            self.period_table.setItem(
                index, 3, QTableWidgetItem(str(period["interrupt"])))
            self.period_table.setItem(
                index, 4, QTableWidgetItem(str(period["sum"])))

            deleteBtn = QPushButton('Xóa')
            deleteBtn.clicked.connect(self.handleDeletePeriod)
            self.period_table.setCellWidget(index, 5, deleteBtn)

    def handleDeletePeriod(self):
        try:
            button = self.sender()
            index = self.period_table.indexAt(button.pos())
            if index.isValid():
                id = self.periods[index.row()]["_id"]
                core.deletePeriodById(id=str(id))
            self.renderPeriodTable()
        except Exception as e:
            print(e)
    def handleDeleteChild(self):
        button = self.sender()
        index = self.childrentable.indexAt(button.pos())
        if index.isValid():
            id = self.childlist[index.row()]["_id"]
            core.deleteChildById(id=str(id))
        self.renderChildrenTable()

    def connectWidgetsSignUp(self):
        self.ui = uic.loadUi('signup.ui', self)
        # connect widgets
        self.username = self.findChild(QLineEdit, 'username')
        self.password = self.findChild(QLineEdit, 'password')
        self.confirmpass = self.findChild(QLineEdit, 'confirmpass')
        self.signup = self.findChild(QPushButton, 'signup')
        self.msg = self.findChild(QLabel, 'msg')
        # connect action
        print(self.signup)
        self.signup.clicked.connect(self.signUpAction)

    def signUpAction(self):
        print('sign up')
        username = self.username.text()
        password = self.password.text()
        confirmpass = self.confirmpass.text()
        if(password != confirmpass):
            self.msg.setText("Password and confirm not same!")
            return
        try:
            userid = core.createAccount(username=username, password=password)
            print("New user created: ", userid['_id'])
            core.saveAccount(str(userid['_id']))
            self.msg.setText("Logged in successfully. Please re-open app!")

        except Exception as ex:
            self.msg.setText(str(ex))
            print(ex)


def main():
    app = QApplication(sys.argv)
    win = Window()
    win.setWindowTitle('Parent app')

    app.exec_()


if __name__ == '__main__':
    main()
